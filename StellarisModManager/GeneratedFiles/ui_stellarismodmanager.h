/********************************************************************************
** Form generated from reading UI file 'stellarismodmanager.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STELLARISMODMANAGER_H
#define UI_STELLARISMODMANAGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StellarisModManagerClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *StellarisModManagerClass)
    {
        if (StellarisModManagerClass->objectName().isEmpty())
            StellarisModManagerClass->setObjectName(QStringLiteral("StellarisModManagerClass"));
        StellarisModManagerClass->resize(600, 400);
        menuBar = new QMenuBar(StellarisModManagerClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        StellarisModManagerClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(StellarisModManagerClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        StellarisModManagerClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(StellarisModManagerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        StellarisModManagerClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(StellarisModManagerClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        StellarisModManagerClass->setStatusBar(statusBar);

        retranslateUi(StellarisModManagerClass);

        QMetaObject::connectSlotsByName(StellarisModManagerClass);
    } // setupUi

    void retranslateUi(QMainWindow *StellarisModManagerClass)
    {
        StellarisModManagerClass->setWindowTitle(QApplication::translate("StellarisModManagerClass", "StellarisModManager", 0));
    } // retranslateUi

};

namespace Ui {
    class StellarisModManagerClass: public Ui_StellarisModManagerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STELLARISMODMANAGER_H
