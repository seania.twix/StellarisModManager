#ifndef STELLARISMODMANAGER_H
#define STELLARISMODMANAGER_H

#include <QtWidgets/QMainWindow>
#include "ui_stellarismodmanager.h"

class StellarisModManager : public QMainWindow
{
	Q_OBJECT

public:
	StellarisModManager(QWidget *parent = 0);
	~StellarisModManager();

private:
	Ui::StellarisModManagerClass ui;
};

#endif // STELLARISMODMANAGER_H
